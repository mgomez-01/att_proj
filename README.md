# Att Project Files
This directory will hold all Att project files that are needed to run the simulation in Feko on a Linux system. Files can be opened in a windows env but will need to be modified for them to work. 

See  [NET file](./att_campus/att_1_campus_lin.net) [NUP file](./att_campus/att_1_campus_lin.nup) 

In these files, you will see an option for each of the ANTENNA sections

```
***** Gain of Antenna *********************************************************
ANTENNA 1 ANTENNA_GAIN_VERTICAL 17.688
***** Horizontal Orientation of the antenna [Degrees] *************************
ANTENNA 1 HORIZONTAL 23.15 N
***** Vertical Orientation of the antenna (Downtilt) [Degrees] ****************
ANTENNA 1 VERTICAL 0.00
***** Electrical Downtilt of the antenna [Degrees] ****************************
ANTENNA 1 ELECTRICAL_DOWNTILT 0.00
***** Transmitted power *******************************************************
ANTENNA 1 POWER 36.00000
***** Power unit **************************************************************
ANTENNA 1 UNIT DBM
```

This is where you can modify the power transmitted. You can change the 36(in dbm) to whatever you choose. also modding the unit to DB, DBM, or W(Watts)


Running the sim can be done as follows if your Altair Feko install is set up correctly

```
WinPropCLI -P --multi-threading N -F att_campus.net
```
Where N is the number of threads you want to use for the calculation. I usually do 2*num_cores of the machine
